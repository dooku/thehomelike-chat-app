# TheHomeLike Assignment App

> Real time chat application created using FeathersJS, MongoDB, ReactJS and Bootstrap.

## About

This project is implementation of requests stated in assignment paper.

To be added:
- Seed data
- Tests (unit on client and server side, integration)
- Deployed app url with user instructions

## Getting Started

1. Clone the repo
2. Install app dependencies

    ```
    cd path/to/thehomelike-chat-app; yarn install
    ```

3. Build the app

    ```
    yarn build
    ```

4. Start the app

    ```
    yarn start
    ```

## App Usage

On app start, the database is seeded with default data generated using FakerJS. 

On the **Login** screen, there is a list of auto-generated user credentials (email and default password) that can be used to log in.

![Login](https://s3.eu-central-1.amazonaws.com/javni/thehomelike/login.png)


New user signup is done on **Signup** screen (all fields are mandatory, including avatar).

![Signup](https://s3.eu-central-1.amazonaws.com/javni/thehomelike/signup.png)


After successful login or signup, the user is granted JWT token and saved to the local storage. 
**Rooms** screen is displayed with all available chat rooms containing basic stats (total number of users and messages).

![Rooms](https://s3.eu-central-1.amazonaws.com/javni/thehomelike/rooms.png)


The room can be deleted clicking on the **Trash** icon, after which the modal is shown to the user, which needs to be confirmed.
![Delete Room](https://s3.eu-central-1.amazonaws.com/javni/thehomelike/room-delete.png)


The new room can be added clicking on **Add new room** box on default room screen. Room name needs to be entered in order to proceed.
![New Room](https://s3.eu-central-1.amazonaws.com/javni/thehomelike/new-room.png)


By clicking on certain room title, the user can enter the chat room. **User List** is shown on the left side, containing all users
participating in the chat. By clicking on **Zoom** icon user can check individual profile page of any of participating users.
![User Profile](https://s3.eu-central-1.amazonaws.com/javni/thehomelike/user-profile.png)


On the right side, there is a **Message List** containing last chat messages.
At the bottom, there is an input box used for entering and sending new message text. 
The user can send image clicking on **Send Image** button.
The user can **Delete** his own messages clicking on trash icon in a message body.

![Chat Screen](https://s3.eu-central-1.amazonaws.com/javni/thehomelike/chat-screen.png)


## Testing

For Feathers auto-generated tests run `npm test` and all tests in the `test/` directory will be run.

