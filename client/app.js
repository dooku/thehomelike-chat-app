import React from 'react';
import ReactDOM from 'react-dom';
import ChatApp from './components/ChatApp';
import './styles/styles.scss';

ReactDOM.render(<ChatApp />, document.getElementById('app'));
