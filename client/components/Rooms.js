import React, { Component } from 'react';
import Modal from 'react-bootstrap-modal';
import client from '../feathers';

import Chat from './Chat';
import Header from './Header';

class Rooms extends Component {
  componentDidMount() {
    const rooms = client.service('/api/rooms');

    // Get all rooms and set state
    rooms.find().then((roomsPage) => {
      const rooms = roomsPage.data;
      this.setState({ rooms });
    });

    // When new room is created add it to rooms list
    rooms.on('created', room => this.setState({
      rooms: this.state.rooms.concat(room)
    }));

    // Remove room from the rooms list
    rooms.on('removed', (removed) => {
      this.setState({ rooms: this.state.rooms.filter(room => room.name !== removed.name) });
    });
  }

  // Set state to render Chat component
  onRoomClick = (e) => {
    const roomName = e.target.id;

    if (roomName) {
      this.setState({ roomName });
      this.render();
    }
  };

  // Set state to render new room form
  onNewRoomClick = () => {
    if (this.state && this.state.newRoom) {
      this.setState({ newRoom: false });
      this.render();
      return;
    }
    this.setState({ newRoom: true });
    this.render();
  };

  // Helper method for saving field value to state
  updateField = (name, ev) => {
    this.setState({ [name]: ev.target.value });
  };

  // Create new room and save state
  createRoom = () => {
    const { name } = this.state;
    if (name) {
      return client.service('api/rooms')
        .create({ name })
        .then(() => {
          this.setState({ name: null, newRoom: null });
          this.render();
        });
    }
    this.setState({ error: { message: 'Missing data' } });
    this.render();
  };

  // Open modal and stop onClick bubbling
  openModal = (e) => {
    const roomId = e.target.id;
    this.setState({ open: true, roomId });
    e.stopPropagation();
  };

  // Close modal and save state
  closeModal = () => this.setState({ open: false });

  // Remove room and save state to close modal
  saveAndCloseModal = () => {
    if (this.state && this.state.roomId) {
      // console.log('roomId', this.state.roomId);
      return client.service('api/rooms')
        .remove({ _id: this.state.roomId })
        .then(() => {
          this.setState({ open: null, roomId: null });
        });
    }
    this.setState({ error: { message: 'Missing data' } });
    this.render();
  };

  render() {
    // Iterating room background colors
    const boxClass = index => `box tile-${index % 5}`;

    // Entering desired chat room
    if (this.state && this.state.roomName) {
      return <Chat roomName={this.state.roomName} />;
    }

    // Creating new chat room
    if (this.state && this.state.newRoom) {
      return (
        <div>
          <Header />
          <div className="container">
            <div className="row vertical-offset-100">
              <div className="col-md-4 col-md-offset-4">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">Create a new room</h3>
                    <div>{this.state.error && this.state.error.message}</div>
                  </div>
                  <div className="panel-body">
                    <form id="room-form">
                      <fieldset>
                        <div className="form-group">
                          <input className="form-control" type="text" name="name" placeholder="room name" onChange={ev => this.updateField('name', ev)} />
                        </div>
                        <div>
                          <button type="button" className="btn btn-success btn-block" onClick={() => this.createRoom()}> Create room </button>
                          <br />
                          <div className="text-center"> <button className="btn btn-default btn-block" onClick={this.onNewRoomClick}><span className="glyphicon glyphicon-chevron-left" /> Go back to the rooms</button> </div>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    // Returning default screen with modal for room delete
    return (
      <div>
        <Header />
        <div className="container vertical-offset-100">
          <div className="row">
            <div className="col-md-12">
              <div className="panel panel-primary">
                <div className="panel-heading">
                  <h3 className="panel-title">
                    <span className="glyphicon glyphicon-th" /> Please choose a room
                  </h3>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-xs-12 col-md-12">
                      <div id="container">
                        { this.state && this.state.rooms.map((room, index) =>
                          (
                            <div className={boxClass(index)} key={room.name}>
                              <span id={room._id} className="glyphicon glyphicon-trash room-delete" onClick={this.openModal} role="button" />
                              <div className="room-title" id={room.name} onClick={this.onRoomClick} role="button">{room.name}</div>
                              <div className="stats">active members: {room.totalUsers || 0}</div>
                              <div className="stats">total messages: {room.totalMessages || 0}</div>
                            </div>
                          ))
                        }
                        <div className="box tile-5" onClick={this.onNewRoomClick} role="button">
                          <div><span className="glyphicon glyphicon-plus" /></div>
                          <div><h4>Add new room</h4></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <Modal show={this.state && this.state.open} onHide={this.closeModal} aria-labelledby="ModalHeader">
            <Modal.Header closeButton>
              <Modal.Title id="ModalHeader">Delete Chat Room?</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>Are you sure that you want to delete chat room? All messages will be automatically deleted as well.</p>
            </Modal.Body>
            <Modal.Footer>
              <Modal.Dismiss className="btn btn-default">Cancel</Modal.Dismiss>
              <button className="btn btn-primary" onClick={this.saveAndCloseModal}>Delete</button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}

export default Rooms;
