import React, { Component } from 'react';
import client from '../feathers';

class Header extends Component {
  componentDidMount() {
    // Get current logged in user data and set personal info
    client.passport.getJWT().then((token) => {
      if (token) {
        client.passport.verifyJWT(token).then((credentials) => {
          if (credentials) {
            client.service('users').get(credentials.userId).then((user) => {
              if (user) {
                this.setState({ user, title: 'Chat App' });
                this.render();
              }
            });
          }
        });
      }
    });
  }

  render() {
    return (
      <div className="header">
        <div>
          <span><img src="/images/thehomelike-logo.svg" height={25} alt="" /></span>
          <span className="header-title"> {this.state && this.state.title} </span>
          <div className="header-user pull-right">
            { this.state && this.state.user && <span> You are signed in as: <span className="header-user-name">{ this.state.user.nickname.toUpperCase() }</span> <button onClick={() => client.logout()} className="btn btn-default"> Sign Out </button> </span> }
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
