import React, { Component } from 'react';
import client from '../feathers';

import Header from './Header';

export default class Login extends Component {
  state = {};
  componentDidMount() {
    this.getEmails();
  }
  // Set state to render signup form
  onClickSignup = () => {
    this.setState({ signup: true, error: null });
    this.clearForm();
    this.render();
  };

  // Set state to render login form
  onClickLogin = () => {
    this.setState({ signup: false, error: null });
    this.clearForm();
    this.render();
  };

  // Trying to authenticate user with supplied credentials
  login = () => {
    const { email, password } = this.state;
    return client.authenticate({
      strategy: 'local',
      email,
      password
    }).catch(error => this.setState({ error }));
  };

  // Signing up user with supplied credentials
  signup = () => {
    const { email, password, nickname, avatar } = this.state;
    if (email && password && nickname && avatar) {
      return client.service('users')
        .create({ email, password, nickname, avatar })
        .then(() => this.login());
    }
    this.setState({ error: { message: 'Missing data' } });
    this.render();
  };

  // Helper method for saving field value to state
  updateField = (name, ev) => {
    this.setState({ [name]: ev.target.value });
  };

  // Helper method for saving user avatar
  updateAvatar = (ev) => {
    const els = document.querySelectorAll('.avatar');
    for (let i = 0; i < els.length; i += 1) {
      els[i].setAttribute('style', 'border:0px;');
    }
    document.getElementById(ev.target.id).style.border = '2px solid grey';
    this.setState({ avatar: `/images/${ev.target.id}.jpg` });
  };

  // Helper method to clean form
  clearForm = () => {
    document.getElementById('sign-form').reset();
  };

  // Get emails list of auto generated users for easier testing
  getEmails = () => {
    const users = client.service('users');
    users.find().then(userList => this.setState({ userList: userList.data }));
  };

  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <div className="row vertical-offset-100">
            <div className="col-md-4 col-md-offset-4">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">{ this.state.signup ? 'Please sign up' : 'Please sign in' }</h3>
                  <div>{this.state.error && this.state.error.message}</div>
                </div>
                <div className="panel-body">
                  <form id="sign-form">
                    <fieldset>
                      <div className="form-group">
                        <input className="form-control" type="email" name="email" placeholder="email" onChange={ev => this.updateField('email', ev)} />
                      </div>
                      <div className="form-group">
                        <input className="form-control" type="password" name="password" placeholder="password" onChange={ev => this.updateField('password', ev)} />
                      </div>

                      { this.state && this.state.signup &&
                      <div>
                        <div className="form-group">
                          <input className="form-control" type="text" name="nickname" placeholder="nickname" onChange={ev => this.updateField('nickname', ev)} />
                        </div>
                        <div id="avatars" className="row">
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user1.jpg" onClick={this.updateAvatar} alt="" id="user1" /></div>
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user2.jpg" onClick={this.updateAvatar} alt="" id="user2" /></div>
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user3.jpg" onClick={this.updateAvatar} alt="" id="user3" /></div>
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user4.jpg" onClick={this.updateAvatar} alt="" id="user4" /></div>
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user5.jpg" onClick={this.updateAvatar} alt="" id="user5" /></div>
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user6.jpg" onClick={this.updateAvatar} alt="" id="user6" /></div>
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user7.jpg" onClick={this.updateAvatar} alt="" id="user7" /></div>
                          <div className="col-md-3 col-sm-3 col-xs-6"><img className="avatar" src="/images/user8.jpg" onClick={this.updateAvatar} alt="" id="user8" /></div>
                        </div>

                        <button type="button" className="btn btn-lg btn-success btn-block" onClick={() => this.signup()}> Signup </button>
                        <div className="text-center"> <a onClick={this.onClickLogin}>Login here!</a> </div>
                      </div>
                      }
                      { !this.state.signup &&
                        <div>
                          <button type="button" className="btn btn-lg btn-success btn-block" onClick={() => this.login()}> Log in </button>
                          <div className="text-center"> <a onClick={this.onClickSignup}>Signup here!</a> </div>
                          <br />
                          <strong>List of seeded user emails:</strong>
                          <div>
                            { this.state && this.state.userList && this.state.userList.map(user =>
                              (
                                <div>{user.email}</div>
                              ))
                            }
                          </div>
                          <br />
                          <strong>Default password: 1234</strong>
                        </div>
                      }
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
