import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import client from '../feathers';

class MessageList extends Component {
  componentDidMount() {
    const messages = client.service('/api/messages')
    // Remove message from the message list
    messages.on('removed', (removedMessage) => {
      this.setState({
        messages: this.props.messages.filter(message => message._id !== removedMessage._id)
      });
    });
  }

  // Delete message
  deleteMessage = (e) => {
    const id = e.target.id;

    return client.service('api/messages')
      .remove({ _id: id })
      .then(() => {
        this.render();
      });
  };
  render() {
    let { messages, user } = this.props;
    // If message is deleted use object from state instead of props
    if (this.state && this.state.messages) {
      messages = this.state.messages;
    }

    return (
      <div id="messages-main" className="chat-message">
        <ul className="chat">
          {messages.map(message =>
            (
              <li key={message._id} className={message.user._id === user._id ? 'left clearfix' : 'right clearfix'}>
              <span className={message.user._id === user._id ? 'chat-img pull-left' : 'chat-img pull-right'}>
                <img src={message.user.avatar} alt="User Avatar" />
              </span>
                <div className="chat-body clearfix">
                  <div className="message__header">
                    { message.user._id === user._id &&
                    <span id={message._id} className="glyphicon glyphicon-trash message-delete" onClick={this.deleteMessage} role="button" />
                    }
                    <strong className="primary-font">{message.user.nickname}</strong>
                    <small className="pull-right text-muted"><i className="fa fa-clock-o" /> { moment.duration(moment(message.createdAt).diff(moment())).humanize(true) } </small>
                  </div>
                  { message.image &&
                  <div className="image-wrapper"><img className="img-thumbnail img-responsive" src={`data:image/jpg;base64,${message.image.toString('base64')}`} alt="Message Photo" /></div>
                  }
                  <p>
                    {message.text}
                  </p>
                </div>
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}


MessageList.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object).isRequired,
  user: PropTypes.object.isRequired
};

export default MessageList;
