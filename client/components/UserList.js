import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap-modal';
import moment from 'moment';
import client from '../feathers';

class UserList extends Component {
  // Get user data in order to show user profile
  getUser = (e) => {
    const id = e.target.id;
    const users = client.service('users');

    if (id) {
      users.get(id).then((user) => {
        this.setState({ user, open: true });
      });
    }
  };

  // Close user profile modal and save state
  closeModal = () => this.setState({ open: false });

  render() {
    const { users, roomName } = this.props;

    return (
      <div id="users-main" className="col-md-4 bg-white user-list"><br />
        <span className="primary-font room-name">{roomName}</span> <span className="room-participants">users ({users.length})</span>
        <hr />
        <ul className="friend-list">
          { users.map(user =>
            (
              <li key={user.email} className="userlist-users">
                <a href="#" className="clearfix">
                  <span id={user._id} className="glyphicon glyphicon-search user-profile" alt="View User Profile" onClick={this.getUser} role="button" />
                  <img src={user.avatar} alt="" className="img-circle" />
                  <div className="user-data">
                    <div className="friend-name" role="button">
                      {user.nickname}
                    </div>
                    <div className="user-email">{user.email}</div>
                  </div>
                </a>
              </li>
            ))
          }
        </ul>
        <br />
        <div>
          <Modal show={this.state && this.state.open} onHide={this.closeModal} aria-labelledby="ModalHeader">
            <Modal.Header closeButton>
              <Modal.Title id="ModalHeader">Profile page for user: { this.state && this.state.user.nickname}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div><span>Avatar: </span> <span><img src={this.state && this.state.user.avatar} alt="" className="img-circle" /></span></div>
              <div><span>Nickname: </span> <span>{ this.state && this.state.user.nickname }</span></div>
              <div><span>E-mail: </span> <span>{ this.state && this.state.user.email }</span></div>
              <div><span>Registered: </span> <span>{ moment.duration(moment(this.state && this.state.user.createdAt).diff(moment())).humanize(true) }</span></div>
            </Modal.Body>
            <Modal.Footer>
              <Modal.Dismiss className="btn btn-default">Ok</Modal.Dismiss>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  roomName: PropTypes.string.isRequired
};

export default UserList;
