import React, { Component } from 'react';
import client from '../feathers';

import Login from './Login';
import Rooms from './Rooms';

class ChatApp extends Component {
  state = {};

  componentDidMount() {
    // Authenticate with the JWT in localStorage
    client.authenticate().catch(() => this.setState({ login: null }));
    // On authenticated set login data to state
    client.on('authenticated', (login) => {
      this.setState({ login });
    });

    // On logout reset all state data - which will then show the login screen
    client.on('logout', () => this.setState({
      login: null
    }));
  }

  render() {
    if (this.state.login === undefined) {
      // Render loading message
      return (
        <main className="container text-center">
          <h1>Loading...</h1>
        </main>
      );
    } else if (this.state.login) {
      // Render room component
      return <Rooms />;
    }
    // Render login screen if user is not authenticated
    return <Login />;
  }
}

export default ChatApp;
