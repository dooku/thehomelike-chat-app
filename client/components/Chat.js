import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fileDialog from 'file-dialog';
import client from '../feathers';

import Header from './Header';
import UserList from './UserList';
import MessageList from './MessageList';
import ChatApp from './ChatApp';

class Chat extends Component {
  componentDidMount() {
    const messages = client.service('/api/messages');
    // Add new messages to the message list
    messages.on('created', (message) => {
      this.setState({ messages: this.state.messages.concat(message) });
      // If user is not in user list add it
      if (this.state && this.state.users && this.state.user) {
        const user = this.state.users.filter(user => user._id === this.state.user._id);
        if (!user || user.length === 0) {
          this.setState({ users: this.state.users.concat(this.state.user) });
        }
      }
      this.scrollToBottom();
    });
    // Get current logged in user
    this.getUser();
    // Get chat data - messages and users
    this.loadChatData(this.props.roomName);
  }

  componentWillUnmount() {
    client.service('/api/messages').removeListener('created', this.scrollToBottom);
  }

  getUser = () => {
    client.passport.getJWT().then((token) => {
      if (token) {
        client.passport.verifyJWT(token).then((credentials) => {
          if (credentials) {
            client.service('users').get(credentials.userId).then((user) => {
              if (user) {
                this.setState({ user });
                this.render();
              }
            });
          }
        });
      }
    });
  };

  loadChatData = (roomName) => {
    const messages = client.service('/api/messages');
    const users = client.service('users');

    Promise.all([
      messages.find({
        query: {
          $sort: { createdAt: -1 },
          $limit: 20,
          room: roomName
        }
      }),
      users.find()
    ]).then(([messagesPage, userPage]) => {
      // We want the messages but in reversed order
      const messages = messagesPage.data.reverse();
      // Get unique users
      const usersRoom = [...new Set(messages.map(message => message.user._id))];
      const users = userPage.data.filter(user => usersRoom.indexOf(user._id) > -1);

      this.setState({ messages, users });
      this.scrollToBottom();
    });
  };

  // Create new chat message
  sendMessage = (ev) => {
    const input = ev.target.querySelector('[name="text"]');
    const text = input.value.trim();

    if (text) {
      client.service('/api/messages').create({ text, room: this.props.roomName }).then(() => {
        input.value = '';
      });
    }
    // Prevent event bubbling
    ev.preventDefault();
  };

  // Set state for room change render
  changeRoom = () => {
    this.setState({ changeRoom: true });
    this.render();
  };

  // Scroll to the bottom of the chat container
  scrollToBottom = () => {
    const node = document.getElementById('bottomScroll');
    node.scrollIntoView({ behavior: 'smooth' });
  };

  // Image upload - image should be jpg, support for other formats coming soon :)
  imageUpload = () => {
    fileDialog({ accept: 'image/jpg' })
      .then((file) => {
        client.service('/api/messages').create({ text: ' ', room: this.props.roomName, image: file[0] }).then(() => {
          input.value = '';
        });
      });
  };

  render() {
    const { roomName } = this.props;

    // Render main screen in order to change room
    if (this.state && this.state.changeRoom) {
      return (
        <ChatApp />
      );
    }

    // Render default chat screen
    return (
      <div>
        <Header />
        <div className="container">
          <div className="row">
            { this.state && this.state.users &&
              <UserList users={this.state.users} roomName={roomName} />
            }
            <main id="mainMessages">
              { this.state && this.state.messages && this.state.user &&
              <MessageList messages={this.state.messages} user={this.state.user} />
              }
              <div id="bottomScroll"></div>
              <div className="chat-box bg-white col-lg-12">
                <span className="d-inline float-left"><button className="btn btn-default" onClick={this.changeRoom}><span className="glyphicon glyphicon-th-large" /> Rooms</button></span>
                <div className="input-group col-lg-7 col-md-9 col-sm-11 col-xs-12 pull-right">
                  <form onSubmit={this.sendMessage} id="send-message" className="input-group">
                    <input className="form-control border no-shadow no-rounded" placeholder="Type your message here" type="text" name="text" />
                    <span className="input-group-btn">
                      <button className="btn btn-success no-rounded" type="submit"><span className="glyphicon glyphicon-ok" /> Send Text</button>
                      <button className="btn btn-default no-rounded" onClick={this.imageUpload}><span className="glyphicon glyphicon-picture" /> Send Image</button>
                    </span>
                  </form>
                </div>
              </div>
            </main>
          </div>
        </div>
      </div>
    );
  }
}

Chat.propTypes = {
  roomName: PropTypes.string.isRequired
};

export default Chat;
