import feathers from 'feathers/client';
import io from 'socket.io-client';
import hooks from 'feathers-hooks';
import socketio from 'feathers-socketio/client';
import authentication from 'feathers-authentication-client';

const client = feathers();
// Set server url
const socket = io('http://localhost:3030');

// Configure middleware
client.configure(hooks());
client.configure(socketio(socket));
client.configure(authentication({
  storage: window.localStorage
}));

export default client;
