const options = {
  services: [
    {
      path: 'api/rooms',
      randomize: false,
      templates: [
        { name: 'FeathersJS' }, { name: 'NodeJS' }, { name: 'Star Wars' }, { name: 'Cats' },
        { name: 'Sports' }, { name: 'Movies' }, { name: 'Programming' }
      ]
    },
    {
      path: 'users',
      count: 10,
      template: {
        email: '{{internet.email}}',
        password: '1234',
        nickname: '{{internet.userName}}',
        avatar: '{{internet.avatar}}'
      },
      callback(user, seed) {
        return seed({
          path: 'api/messages',
          count: 5,
          templates: [
            {
              user: user.email,
              room: 'FeathersJS',
              text: '{{lorem.sentences}}'
            },
            {
              user: user.email,
              room: 'NodeJS',
              text: '{{lorem.sentence}}'
            },
            {
              user: user.email,
              room: 'Star Wars',
              text: '{{lorem.word}}'
            },
            {
              user: user.email,
              room: 'Movies',
              text: '{{lorem.word}}'
            },
            {
              user: user.email,
              room: 'Cats',
              text: '{{company.catchPhrase}}'
            },
            {
              user: user.email,
              room: 'FeathersJS',
              text: '{{lorem.word}}'
            },
            {
              user: user.email,
              room: 'Movies',
              text: '{{lorem.words}}'
            },
            {
              user: user.email,
              room: 'Programming',
              text: '{{lorem.sentence}}'
            },
            {
              user: user.email,
              room: 'Programming',
              text: '{{lorem.sentences}}'
            },
            {
              user: user.email,
              room: 'Cats',
              text: '{{lorem.words}}'
            }
          ]
        });
      }
    }
  ]
};

module.exports = options;
