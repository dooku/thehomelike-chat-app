// Hook that adds room stats to results object
module.exports = function () {
  return function getCount(hook) {
    return new Promise((resolve) => {
      let queue = Promise.resolve();

      hook.result.data.forEach((room, index) => {
        queue = queue.then(() => {
          return hook.app.services['api/messages'].find({ query: { room: room.name } }).then((messages) => {
            hook.result.data[index].totalMessages = messages.total;
            hook.result.data[index].totalUsers = [...new Set(messages.data.map(message => message.user._id.toString()))].length;
          });
        });
      });

      resolve(queue);
    });
  };
};
