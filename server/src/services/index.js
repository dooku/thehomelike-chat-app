const messages = require('./messages/messages.service.js');
const rooms = require('./rooms/rooms.service.js');
const users = require('./users/users.service.js');

module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(messages);
  app.configure(rooms);
  app.configure(users);
};
