const hooks = require('feathers-hooks-common');
const { associateCurrentUser } = require('feathers-authentication-hooks');

const userMessageSchema = {
  include: {
    service: 'users',
    nameAs: 'user',
    parentField: 'user',
    childField: 'email'
  }
};

module.exports = {
  before: {
    all: [],
    find: [associateCurrentUser({ idField: 'email', as: 'user' })],
    get: [],
    create: [associateCurrentUser({ idField: 'email', as: 'user' })],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [hooks.populate({ schema: userMessageSchema })],
    get: [],
    create: [hooks.populate({ schema: userMessageSchema })],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
